import './Details.css'
import webcam from '../../Assets/webcam.png'
import hotel from '../../Assets/hotel.png'
import creditCard from '../../Assets/creditCard.png'
import dr from '../../Assets/dr.png'
import chica from '../../Assets/chica1.jpg'


function Details(){
    return (
        <div className="details">
            <div className="details__left">
                <div className="left__options">
                    <div className="left__optionsOne">
                        <img src={webcam} className="img__webcam"/>
                        <img src={hotel} className="img__hotel"/>

                    </div>
                    <div className="left__optionsTwo">
                        <img src={creditCard} className="img__creditCard"/>
                        <img src={dr} className="img__dr"/>

                    </div>

                </div>

                <img src={chica} className="left__main"/>

                <div className="left__footer">
                    <img src={chica}/>
                    <img src={chica}/>
                    <img src={chica}/>
                    <img src={chica}/>


                </div>

            </div>


                     <div className="details__right">


                    <div className="right__top">
                        <div className="right__top--stars">
                            <span>★</span>
                            <span>★</span>
                            <span>★</span>
                            <span>★</span>
                            <span>★</span>

                        </div>
                        <span className="right__top--name">
                        Nombre
                    </span>
                        <p className="right__top--description">
                            Is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since
                            the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries,
                            but also the leap into electronic typesetting, remaining essentially unchanged

                        </p>
                    </div>

                    <div className="right__bottom">
                        Reservar
                    </div>




            </div>






        </div>
    )
}

export default Details