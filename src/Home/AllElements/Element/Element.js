import './Element.css';
import webcam from '../../../Assets/webcam.png'
import hotel  from '../../../Assets/hotel.png'
import creditCard from '../../../Assets/creditCard.png'
import dr from '../../../Assets/dr.png'

function Element() {

    return (
        <div  className="element">
            <div className="element__nueva">Nueva</div>


            <div className="element__footer">
                <div className="element_options">
                    <div className="element__optionsOne">
                        <img className="img__webcam" src={webcam} alt=""/>
                        <img className="img__hotel" src={hotel} alt=""/>

                    </div>
                    <div className="element__optionsTwo">
                        <img className="img__creditCard" src={creditCard} alt=""/>
                        <img className="img__dr" src={dr} alt=""/>

                    </div>
                </div>
                <div className="element__stateContainer">
                    <div>
                        Disponible
                    </div>
                </div>
            </div>

        </div>
    )

}

export default Element;