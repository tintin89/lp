import './AllElements.css'
import Element from "./Element/Element";


function AllElements() {

    return (
        <div className="allElements">
            <Element/>
            <Element/>
            <Element/>
            <Element/>
            <Element/>
            <Element/>
            <Element/>
            <Element/>
            <Element/>
            <Element/>
        </div>
    )

}

export default AllElements