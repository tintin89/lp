import './NavBar.css'


function NavBar() {

    return (
        <div className="navBar">
            <div className="navBar__groupOne">
                <div style={{color:"#8F1414"}}>INICIO</div>
                <div>IQUIQUE</div>
            </div>
            <div className="navBar__groupTwo">
                <div>ENTRAR</div>
            </div>
        </div>
    )

}

export default NavBar