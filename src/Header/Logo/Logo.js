import './Logo.css'
import logoImg from '../../Assets/imagen_logo.png'


function Logo() {

    return (
        <div className="logo">
            <div className="logo__title">
                <div className="logo__este">Este es un</div>
                <div className="logo__lugar">LugarDePLacer.com</div>
            </div>
            <img src={logoImg} className="logo__image"/>
        </div>
    )

}

export default Logo